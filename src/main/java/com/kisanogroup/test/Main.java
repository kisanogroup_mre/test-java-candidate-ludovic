/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kisanogroup.test;

import com.kisanogroup.test.httpserver.HttpServer;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ixm
 */
public class Main {
    private final Logger logger = LogManager.getLogger(Main.class);

    private static final List<Integer> portRange = List.of(
        2375, 2376, 2377, 2378, 2379, 2380
    );

    public static void main(String[] args) {
        HttpServer.startServer();

        new Main(args[0], args[1]);
    }

    public Main(String name, String filePath) {
    }
}
